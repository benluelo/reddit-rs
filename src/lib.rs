#![deny(clippy::disallowed_method, clippy::unwrap_used)]
#![warn(clippy::pedantic)]
#![allow(clippy::module_name_repetitions, clippy::option_option)]
#![cfg_attr(not(test), warn(clippy::dbg_macro))]
#![cfg_attr(
    test,
    allow(clippy::semicolon_if_nothing_returned, clippy::unwrap_used)
)] // for dbg! macro in tests

mod client;
pub mod models;
mod utils;

pub use client::{Config, RedditClient, RedditError};
