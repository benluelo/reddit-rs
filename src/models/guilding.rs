use std::{collections::HashMap, fmt::Display, num::ParseIntError, str::FromStr};

use serde::{Deserialize, Deserializer, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Default)]
pub struct Gildings(HashMap<Gid, u16>);

/// ```text
/// "gid_1"
///      ^ value
/// ```
/// REVIEW: <https://www.reddit.com/r/redditdev/comments/9ipema/new_parameter_in_comment_and_submission_objects/>
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Gid {
    value: u16,
}

impl Serialize for Gid {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl<'de> Deserialize<'de> for Gid {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        Gid::from_str(&String::deserialize(deserializer)?).map_err(serde::de::Error::custom)
    }
}

impl Display for Gid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("gid_{}", self.value))
    }
}

impl FromStr for Gid {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (_, value) = s
            .split_once('_')
            .ok_or_else(|| "expected an underscore in gid key".to_string())?;

        Ok(Gid {
            value: value
                .parse()
                .map_err(|val: ParseIntError| val.to_string())?,
        })
    }
}
