use crate::models::{author::AuthorFlairTemplateId, link::LinkFlairTemplateId, private::Sealed};

pub trait TemplateId: Copy + Sealed {}

impl Sealed for AuthorFlairTemplateId {}

impl TemplateId for AuthorFlairTemplateId {}

impl Sealed for LinkFlairTemplateId {}

impl TemplateId for LinkFlairTemplateId {}
