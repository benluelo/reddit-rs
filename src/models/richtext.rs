use iri_string::types::IriAbsoluteString;
use serde::{Deserialize, Serialize};

use crate::models::reddit_emoji::RedditEmoji;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "snake_case")]
#[serde(tag = "e")]
pub enum Richtext {
    Emoji {
        a: RedditEmoji,
        u: IriAbsoluteString,
    },
    Text {
        t: String,
    },
}
