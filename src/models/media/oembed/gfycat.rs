use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub struct Gfycat {
    pub title: String,
    pub description: String,
    pub author_name: String,
}
