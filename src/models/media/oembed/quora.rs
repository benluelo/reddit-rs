use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub struct Quora {
    pub mean_alpha: f64,
    pub author_name: String,
}
