use serde::{Deserialize, Serialize};

use iri_string::types::IriString;

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub struct YouTubeMobile {
    pub description: String,
    pub author_name: String,
    pub author_url: IriString,
}
