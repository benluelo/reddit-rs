use chrono::{DateTime, Utc};
use iri_string::types::IriString;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub struct Twitter {
    pub url: IriString,
    pub author_name: String,
    pub author_url: IriString,
    #[serde(with = "chrono::serde::ts_seconds")]
    pub cache_age: DateTime<Utc>,
}
