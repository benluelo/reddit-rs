use serde::{Deserialize, Serialize};

use iri_string::types::IriString;

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub struct VimeoPlayer {
    pub title: String,
    pub author_name: String,
    pub author_url: IriString,
}
