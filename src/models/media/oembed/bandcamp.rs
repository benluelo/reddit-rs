use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub struct Bandcamp {
    pub title: String,
    pub description: String,
}
