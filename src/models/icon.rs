use iri_string::types::IriString;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum IconFormat {
    Png,
    Apng,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Icon {
    url: IriString,
    width: u16,
    height: u16,
    format: Option<IconFormat>,
}
