use std::collections::HashMap;

use iri_string::types::IriAbsoluteString;
use serde::{de::Unexpected, Deserialize, Serialize};

use crate::models::{
    fullname::FullName,
    icon::{Icon, IconFormat},
};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Awarding {
    giver_coin_reward: Option<u16>,
    subreddit_id: Option<FullName>,
    is_new: bool,
    days_of_drip_extension: u32,
    coin_price: u32,
    /// REVIEW: award_483d8e29-bbe5-404e-a09a-c2d7b16c4fff
    id: String,
    penny_donate: Option<u32>,
    award_sub_type: AwardSubType,
    coin_reward: u32,
    icon_url: IriAbsoluteString,
    days_of_premium: u32,
    tiers_by_required_awardings: Option<TiersByRequiredAwardings>,
    resized_icons: Vec<Icon>,
    resized_static_icons: Vec<Icon>,
    icon_width: u32,
    static_icon_width: u32,
    start_date: Option<()>,
    is_enabled: bool,
    awardings_required_to_grant_benefits: Option<u32>,
    description: String,
    end_date: Option<()>,
    subreddit_coin_reward: u32,
    count: u32,
    static_icon_height: u32,
    name: String,
    icon_format: Option<IconFormat>,
    icon_height: u32,
    penny_price: Option<u32>,
    award_type: AwardType,
    static_icon_url: IriAbsoluteString,
}

#[derive(Debug, Clone, Serialize, PartialEq)]
pub struct TiersByRequiredAwardings(HashMap<u32, RequiredAwardings>);

impl<'de> Deserialize<'de> for TiersByRequiredAwardings {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        use serde::de::Error;

        let map = HashMap::<String, RequiredAwardings>::deserialize(deserializer)?;

        Ok(Self(
            map.into_iter()
                .map(|(k, v)| match k.parse() {
                    Ok(i) => Ok((i, v)),
                    Err(_why) => Err(D::Error::invalid_value(
                        Unexpected::Str(&k),
                        &"a numeric string key",
                    )),
                })
                .collect::<Result<HashMap<u32, RequiredAwardings>, _>>()?,
        ))
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct RequiredAwardings {
    resized_icons: Vec<Icon>,
    resized_static_icons: Vec<Icon>,
    awardings_required: u32,
    icon: Icon,
    static_icon: Icon,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum AwardSubType {
    Global,
    Premium,
    Group,
    Appreciation,
    Community,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "snake_case")]
pub enum AwardType {
    Global,
    Community,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum TopAwardedType {
    Active,
    Inactive,
}
