use chrono::{DateTime, Utc};
use iri_string::types::IriRelativeString;
use serde::{Deserialize, Serialize};

use crate::models::{
    author::Author, award::TopAwardedType, fullname::FullName, guilding::Gildings,
    link::SubredditInfo,
};

#[allow(clippy::struct_excessive_bools)] // reddit api sucks
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Comment {
    all_awardings: Vec<()>,
    approved_at_utc: Option<()>,
    approved_by: Option<()>,
    archived: bool,
    associated_award: Option<()>,
    #[serde(flatten)]
    author: Author,
    awarders: Vec<()>,
    banned_at_utc: Option<()>,
    banned_by: Option<()>,
    body_html: String,
    body: String,
    can_gild: bool,
    can_mod_post: bool,
    collapsed_because_crowd_control: Option<()>,
    collapsed_reason_code: Option<()>,
    collapsed_reason: Option<()>,
    collapsed: bool,
    comment_type: Option<()>,
    controversiality: u32,

    #[serde(deserialize_with = "crate::utils::integer_or_float_to_datetime")]
    created_utc: DateTime<Utc>,
    #[serde(deserialize_with = "crate::utils::integer_or_float_to_datetime")]
    created: DateTime<Utc>,
    depth: u32,
    distinguished: Option<()>,
    downs: u32,
    edited: bool,
    gilded: u32,
    gildings: Gildings,
    /// REVIEW: base64?
    id: String,
    is_submitter: bool,
    likes: Option<()>,
    link_id: FullName,
    locked: bool,
    mod_note: Option<()>,
    mod_reason_by: Option<()>,
    mod_reason_title: Option<()>,
    mod_reports: Vec<()>,
    name: FullName,
    no_follow: bool,
    num_reports: Option<()>,
    parent_id: FullName,
    permalink: IriRelativeString,
    removal_reason: Option<()>,
    replies: String,
    report_reasons: Option<()>,
    saved: bool,
    score_hidden: bool,
    score: u32,
    send_replies: bool,
    stickied: bool,
    #[serde(flatten)]
    subreddit: SubredditInfo,
    top_awarded_type: Option<TopAwardedType>,
    total_awards_received: u32,
    treatment_tags: Vec<()>,
    unrepliable_reason: Option<()>,
    ups: u32,
    user_reports: Vec<()>,
}
