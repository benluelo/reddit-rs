use serde::{Deserialize, Serialize};

use crate::{
    models::{
    link::{LinkFlair, LinkFlairTemplateId},
        author::{AuthorFlair, AuthorFlairTemplateId},
        color::Color,
        flair::template_id::TemplateId,
        private::Sealed,
        richtext::Richtext,
    },
};

pub mod template_id;

pub trait Flair: Sealed {
    type TemplateId: TemplateId;

    fn background_color(&self) -> Option<Color>;

    fn template_id(&self) -> Option<Self::TemplateId>;

    fn css_class(&self) -> Option<&str>;

    fn richtext(&self) -> Option<&[Richtext]>;

    fn ty(&self) -> Option<FlairType>;

    fn text(&self) -> Option<&str>;
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "snake_case")]
pub enum FlairType {
    Text,
    Richtext,
}

#[derive(
    Debug, Clone, Copy, Serialize, Deserialize, PartialEq, strum::AsRefStr, strum::EnumString,
)]
#[serde(rename_all = "snake_case")]
#[strum(serialize_all = "snake_case")]
pub enum FlairTextColor {
    Light,
    Dark,
}

impl Sealed for AuthorFlair {}

impl Flair for AuthorFlair {
    type TemplateId = AuthorFlairTemplateId;

    fn background_color(&self) -> Option<Color> {
        self.background_color
    }

    fn template_id(&self) -> Option<Self::TemplateId> {
        self.template_id
    }

    fn css_class(&self) -> Option<&str> {
        self.css_class.as_deref()
    }

    fn richtext(&self) -> Option<&[Richtext]> {
        self.richtext.as_deref()
    }

    fn ty(&self) -> Option<FlairType> {
        self.ty
    }

    fn text(&self) -> Option<&str> {
        self.text.as_deref()
    }
}

impl Sealed for LinkFlair {}

impl Flair for LinkFlair {
    type TemplateId = LinkFlairTemplateId;

    fn background_color(&self) -> Option<Color> {
        self.background_color
    }

    fn template_id(&self) -> Option<Self::TemplateId> {
        self.template_id
    }

    fn css_class(&self) -> Option<&str> {
        self.css_class.as_deref()
    }

    fn richtext(&self) -> Option<&[Richtext]> {
        Some(&self.richtext)
    }

    fn ty(&self) -> Option<FlairType> {
        Some(self.ty)
    }

    fn text(&self) -> Option<&str> {
        self.text.as_deref()
    }
}
