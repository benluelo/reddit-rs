use std::{fmt::Display, str::FromStr};

use serde::{Deserialize, Deserializer, Serialize, Serializer};

#[derive(Debug, Clone, PartialEq)]
pub struct RedditEmoji(String);

impl<'de> Deserialize<'de> for RedditEmoji {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        RedditEmoji::from_str(&String::deserialize(deserializer)?).map_err(serde::de::Error::custom)
    }
}

impl Serialize for RedditEmoji {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl FromStr for RedditEmoji {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with(':') && s.ends_with(':') {
            Ok(Self(s[1..s.len() - 1].to_string()))
        } else {
            Err("improperly formatted emoji".to_string())
        }
    }
}

impl Display for RedditEmoji {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(":{}:", self.0))
    }
}
